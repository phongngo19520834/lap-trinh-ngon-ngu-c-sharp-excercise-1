﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace bt
{
    public partial class Hotel : Form
    {
        DataTable dt = new DataTable();
        private int SelectedRowIndex;
        public Hotel()
        {
            InitializeComponent();
            DataGridView_HotelList.Columns.Clear();
            dt.Columns.Add("Ma", typeof(string));
            dt.Columns.Add("Loai", typeof(string));
            dt.Columns.Add("Gia", typeof(double));
            dt.Columns.Add("TT", typeof(string));
            dt.Columns.Add("Ngay", typeof(int));
        }

        private void btn_Them_Click(object sender, EventArgs e)
        {
            try
            {
                string ma, loai, tt;
                int ngay;
                double gia;
                ma = txt_MaPhong.Text;
                loai = cbo_LoaiPhong.Text;
                gia = Convert.ToDouble(txt_GiaPhong.Text);
                tt = cbo_TTPhong.Text;
                ngay = Convert.ToInt32(txt_NgayO.Text);
                dt.Rows.Add(ma, loai, gia, tt, ngay);

                DataGridView_HotelList.DataSource = dt;
            }
            catch { MessageBox.Show("Dữ liệu nhập vào có lỗi !!!"); };

        }

        private void dataGridView1_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            SelectedRowIndex = e.RowIndex;
            DataGridViewRow CurrentRow = DataGridView_HotelList.Rows[SelectedRowIndex];
            txt_MaPhong.Text = CurrentRow.Cells[0].Value.ToString();
            cbo_LoaiPhong.Text = CurrentRow.Cells[1].Value.ToString();
            txt_GiaPhong.Text = CurrentRow.Cells[2].Value.ToString();
            cbo_TTPhong.Text = CurrentRow.Cells[3].Value.ToString();
            txt_NgayO.Text = CurrentRow.Cells[4].Value.ToString();

        }

        private void btn_Dat_Click(object sender, EventArgs e)
        {
            try
            {
                DataGridViewRow NewRow = DataGridView_HotelList.Rows[SelectedRowIndex];
                NewRow.Cells[3].Value = "Không trống";
                NewRow.Cells[4].Value = Convert.ToInt32(txt_NgayO.Text);
                cbo_TTPhong.Text = "Không trống";
            }
            catch { MessageBox.Show("Dữ liệu nhập vào có lỗi !!!"); };
        }

        private void btn_TToan_Click(object sender, EventArgs e)
        {
            DataGridViewRow SeletedRow = DataGridView_HotelList.Rows[SelectedRowIndex];
            try
            {
                double Cost =Convert.ToDouble(SeletedRow.Cells[2].Value)*Convert.ToDouble(SeletedRow.Cells[4].Value);
                MessageBox.Show($"Tổng số tiền:  {Cost}");
            }
            catch { MessageBox.Show("Dữ liệu nhập vào có lỗi ! = !!"); };

        }

        private void btn_Huy_Click(object sender, EventArgs e)
        {
            DataGridViewRow NewRow = DataGridView_HotelList.Rows[SelectedRowIndex];
            NewRow.Cells[3].Value = "Trống";
            NewRow.Cells[4].Value = 0;
            cbo_TTPhong.Text = "Trống";
            txt_NgayO.Text = "0";
        }
    }
}
